<?php
    // zaladowanie configu aplikacji
    require 'application/config/config.php';
    require 'application/models/basemodel.php';
    require 'application/libs/controller.php';
    require 'application/controller/session.php';

    $session_obj = new Session();
    $session_obj->start();

    // zaladowanie potrzebnych klas
    require 'application/libs/menu.php';
    require 'application/libs/layout.php';

    require 'application/libs/application.php';

    $app = new Application();


