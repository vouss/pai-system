<?php

class Session extends Controller {

    public $ses_obj = null;

    public function start() {
        $this->ses_obj = $this->loadModel("SessionModel");
        session_start();

        if(isset($_SESSION['verification'])) {
            if($this->checkSession()) {
                $this->deleteFromDb();
                $_SESSION['verification'] = hash("sha256",$_SERVER['HTTP_USER_AGENT'] . $_SERVER['SERVER_ADDR']. session_id());
                $this->saveToDb();
            } else {
                $this->deleteFromDb();
                $this->destroy();
            };
        }
    }

    public function saveToDb() {
        $this->ses_obj->addSession($_SESSION['verification']);
    }

    public function deleteFromDb() {
        if(isset($_SESSION['verification'])) {
            $this->ses_obj->deleteSession($_SESSION['verification']);
        }
    }

    public function checkSession() {
        if($this->ses_obj->checkSession($_SESSION['verification'])) {
            return true;
        }

        return false;
    }

    public function set($name, $value) {
        $_SESSION[$name] = $value;
    }

    public function is($name, $status) {
        return (isset($_SESSION[$name]) && $_SESSION[$name] == $status)? true : false;
    }

    public function get($name) {
        return ($this->exist($name))? $_SESSION[$name] : false; //nie istnieje sesja!
    }

    public function clear($name) {
        unset($_SESSION[$name]);
    }

    public function clearAll() {
        $_SESSION = array();
    }

    public function destroy() {
        $this->clearAll();
        if ( isset( $_COOKIE[session_name()] ) )
            setcookie( session_name(), "", time()-3600, "/" );
        $_SESSION = array();
    }

    function check_session() {
        if(!isset($_SESSION['created'])) {
            session_regenerate_id();
            $_SESSION['created'] = true;
        }


        if(!isset($_SESSION['verification'])) {
            $_SESSION['verification'] = hash("sha256",$_SERVER['HTTP_USER_AGENT'] . $_SERVER['SERVER_ADDR']);

        } else if($_SESSION['verification'] != hash("sha256",$_SERVER['HTTP_USER_AGENT'] . $_SERVER['SERVER_ADDR'])) {
            return false;
        }

        return true;
    }
}