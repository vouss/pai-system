<?php

class Login extends Controller {

    public function index() {

        if(!$GLOBALS["session_obj"]->is("status", "zalogowany")) {
            require 'application/views/auth/login.php';
        } else {
            header("Location: main");
        }
    }

    public function log() {
        global $session_obj;

        if ($this->isMethod('POST')) {
            $login_model = $this->loadModel('LoginModel');
            if($login_model->login()) {
                $users_model = $this->loadModel('UsersModel');
                $data = $users_model->userCheck($_POST['login']);
                session_start();
                foreach($data as $row) {
                    $_SESSION['status'] = "zalogowany";
                    $_SESSION['name'] =  $row->imie . " " . $row->nazwisko;
                    $_SESSION['email'] =  $row->email;
                    $_SESSION['group'] =  $row->nazwa_grupy;
                    $_SESSION['id'] = $row->id_uzytkownika;
                }

                $session_obj->set('verification',hash("sha256",$_SERVER['HTTP_USER_AGENT'] . $_SERVER['SERVER_ADDR']. session_id()));
                $session_obj->saveToDb();
                echo json_encode(true);

            } else {
                echo json_encode(false);
            }
            //echo ($reg_status)? json_encode(true) : json_encode(false); //ma zwracac konrektna grupe! redirect w widoku!

        }
    }
    public function logout() {
        global $session_obj;

        $session_obj->deleteFromDb();
        $session_obj->destroy();
        header("Location: ../login");
    }
}

?>