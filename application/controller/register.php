<?php

class Register extends Controller {

    public function index() {
        if(isset($_SESSION['status']) && $_SESSION['status'] == 'zalogowany') {
            require 'application/views/_templates/header.php';
            require 'application/views/home/index.php';
            require 'application/views/_templates/footer.php';
        } else {
            $regModel = $this->loadModel('UsersModel');
            $country = $regModel->getCountry();
            require 'application/views/auth/register.php';
        }
    }

    public function check() {
        if ($this->isAjax()) {
            if (empty($_POST['mail'])) {
                echo json_encode('Puste pole e-mail!');
            } else {
                $regModel = $this->loadModel('UsersModel');
                $mail = $regModel->checkMail($_POST['mail']);
                echo ($mail)? json_encode('E-mail jest wolny!') : json_encode('E-mail jest zajęty!');
            }
            return true;
        }
    }

    //array(11) {
    //["name"]=>
    //string(4) "qqqq"
    //["surname"]=>
    //string(10) "cxzczxczxc"
    //["email"]=>
    //string(9) "www@ww.pl"
    //["tel"]=>
    //string(10) "2434234324"
    //["pass1"]=>
    //string(9) "Qwerty12!"
    //["pass2"]=>
    //string(9) "Qwerty12!"
    //["street"]=>
    //string(5) "Jasna"
    //["home_number"]=>
    //string(2) "22"
    //["code_number"]=>
    //string(6) "21-200"
    //["city"]=>
    //string(8) "Wrocław"
    //["country"]=>
    //string(1) "2"
    //}
    public function create() {

        if($this->isMethod('POST')) {
            $regModel = $this->loadModel('UsersModel');

            $register = $regModel->addUser($_POST);

            echo ($register)? json_encode(true) : json_encode(false);

        }
    }
}