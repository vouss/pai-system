<?php

class Main extends Controller {

    public $layout = null;

    private function createLayout($uprawnienia_strony,$data,$data2,$funkcja) {

        if(isset($_SESSION['status'])) {
            if($_SESSION['status'] == 'zalogowany') {
                $this->layout = new Layout($uprawnienia_strony,$this->getPrivilegesFromGroup($_SESSION['group']),$funkcja, $data,$data2);
                $this->layout->make();
            }
        } else {
            header("Location: ../login");
        }

    }
    public function index() {
        $this->createLayout(1023,null,null,__FUNCTION__);
    }

    public function projects() {
        $projectsModel = $this->loadModel("ProjectsModel");
        if($this->isAjax()) {

        } else {
            if ($this->isMethod("GET")) {
                $data = $projectsModel->printAllProjects();
//
//
                $this->createLayout(496,$data,null,__FUNCTION__);
            }
        }
    }

    public function workerProjects() {

        $workerProjects = $this->loadModel("ProjectsModel");
        $data = $workerProjects->workerProjects($_SESSION['id']);
        $this->createLayout(1023,$data,null,__FUNCTION__);
    }

    public function workerProjectGroups() {

        $workerGroupsModel = $this->loadModel("ProjectGroupsModel");

        $data = $workerGroupsModel->printWorkerGroups($_SESSION['id']);
        $this->createLayout(1023,$data,null,__FUNCTION__);
    }

    public function sentOrders() {
        $order_model = $this->loadModel("OrdersModel");

        $data = $order_model->sentOrders($_SESSION['id']);
        $this->createLayout(1023,$data,null,__FUNCTION__);
    }

    public function profile() {
        $usersModel = $this->loadModel("UsersModel");
        //$this->groups = $this->loadModel("GroupsModel");
        if ($this->isAjax()) {
            $_POST['id'] = $_SESSION['id'];
            $status = $usersModel->editUser($_POST);
            if($status) {
                echo json_encode(true);
            } else {
                echo json_encode(false);
            }
        } else {
            if ($this->isMethod("GET")) {
                $data = array();
                $data[0] = $usersModel->userData($_SESSION['id']);
                $data[1] = $usersModel->getCountry();
                $this->createLayout(1023,$data,null,__FUNCTION__);
            }
        }

    }

    public function projectGroups() {

        $projectGroups = $this->loadModel("ProjectGroupsModel");
        $usersModel = $this->loadModel("UsersModel");
        $projectsModel =$this->loadModel("ProjectsModel");
        if ($this->isAjax()) {

            if($_POST['action'] == 'dodaj') {
                $tmp = filter_var($_POST['groupName'],FILTER_SANITIZE_STRING);
                echo $tmp;
                if(!(strlen($tmp)<3 && strlen($tmp)>20)) {
                    $status = $projectGroups->addProjectGroup($tmp, $_POST['proj']);
                }

                return json_encode(true);
            } elseif($_POST['action'] == 'usun') {
                $status = $projectGroups->removeProjectGroup($_POST['id']);

                return json_encode(true);
            } elseif($_POST['action'] == 'info') {
                $data = $projectGroups->printProjectGroup($_POST['id']);

                echo json_encode($data);
            } elseif($_POST['action'] == 'przydziel') {
                $status = $projectGroups->addPersonToProjectGroup($_POST['person_id'], $_POST['group_id']);

                echo json_encode($status);
            } elseif($_POST['action'] == 'usunOsobeZGrupy') {
                $status = $projectGroups->removePersonFromProjectGroup($_POST['id_uzytkownika'], $_POST['id_grupy']);

                echo json_encode($status);
            }

            return false;

        } elseif ($this->isMethod("GET")) {
            $data2 = array();
            $data2[0] = $usersModel->printAllByType('pracownicy');
            $data2[1] = $projectsModel->printAllProjects();
            $data = $projectGroups->printAllGroups();

            $this->createLayout(1023,$data,$data2,__FUNCTION__);
        }
    }

    public function sendOrder() {
        $order_model = $this->loadModel("OrdersModel");
        $service_model = $this->loadModel("ServicesModel");
        if($this->isAjax()) {
            if($_POST['action'] == 'dodaj') {
                $order_model->addOrder($_POST);

                json_encode(true);
            }
        } else {
            if ($this->isMethod("GET")) {
                $data = $service_model->printAllServices();

                $this->createLayout(1023,$data,null,__FUNCTION__);
            }
        }
    }

    public function orders() {

        $order_model = $this->loadModel("OrdersModel");
        if($this->isAjax()) {
            if($_POST['action'] == 'usun') {
                $status = $order_model->removeOrder($_POST['id']);
                echo ($status)? json_encode(true) : json_encode(false);
            } elseif($_POST['action'] == 'edytuj') {
//                $status = $user_model->editUser($_POST);
//                echo ($status)? json_encode(true) : json_encode(false);
            } elseif($_POST['action'] == 'zatwierdz') {
                 $order = $order_model->printOrder($_POST['id']);

                $status = $order_model->orderToProject($order);
                echo ($status)? json_encode(true) : json_encode(false);

            }

        } else {
            if ($this->isMethod("GET")) {
                $data = $order_model->printAllOrders();
                $this->createLayout(496,$data,null,__FUNCTION__);
            }
        }
    }

    public function privilageGroups() {

        $data = $this->groups->printAllGroups();
        $this->createLayout(496,$data,null,__FUNCTION__);
    }

    public function services() {
        $service_model = $this->loadModel("ServicesModel");

        if($this->isAjax()) {
            if ($this->isMethod("POST")) {
                if($_POST['action'] == 'usun') {
                    $status = $service_model->removeService($_POST['id']);
                    echo ($status)? json_encode(true) : json_encode(false);
                } elseif($_POST['action'] == 'dodaj') {

                    $status = $service_model->addService($_POST);

                    echo ($status)? json_encode(true) : json_encode(false);
                } elseif($_POST['action'] == 'pobierz') {
                    $data = $service_model->printService($_POST['id']);

                    echo json_encode($data);
                } elseif($_POST['action'] == 'edytuj') {
                    $status = $service_model->editService($_POST);

                    echo ($status)? json_encode(true) : json_encode(false);
                }
            }
        } else {
            if ($this->isMethod("GET")) {
                $data = $service_model->printAllServices();

                $this->createLayout(496,$data,null,__FUNCTION__);
            }
        }
    }

    public function users() {

        $user_model = $this->loadModel("UsersModel");

        if($this->isAjax()) {
            if ($this->isMethod("POST")) {
                if($_POST['action'] == 'usun') {
                    $status = $user_model->removeUser($_POST['id']);
                    echo ($status)? json_encode(true) : json_encode(false);
                } elseif($_POST['action'] == 'zmienGrupe') {
                    $status = $user_model->changeGroup($_POST['group'],$_POST['id']);

                    echo json_encode($status);
                } elseif($_POST['action'] == 'edytuj') {
                    $status = $user_model->editUser($_POST);

                    echo ($status)? json_encode(true) : json_encode(false);
                } elseif($_POST['action'] == 'pobierz') {
                    $data = $user_model->userData($_POST['id']);
                    echo json_encode($data);
                }
            }
        } else {
            if ($this->isMethod("GET")) {
                $data = array();
                $data[0] = $user_model->printAllUsers();
                $data[1] = $user_model->getCountry();
                $gr = $this->groups->printAllGroups();
//                var_dump($data);
                $this->createLayout(496,$data,$gr,__FUNCTION__);
            }
        }

    }

    private function getPrivilegesFromGroup($group_name) {
        if($_SESSION['status'] == "zalogowany") {
            $privileges = null;
            $data = $this->groups->printGroup($group_name);

            foreach ($data as $row) {
                $privileges = $row->uprawnienia;
            }

            return $privileges;
        }
    }

}