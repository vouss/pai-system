<!DOCTYPE html>
<html>
<head lang="pl">
    <meta charset="UTF-8">
    <title>SystemINiT</title>
    <link href="<?php echo URL; ?>public/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL; ?>public/css/style.css" rel="stylesheet">
    <script src="<?php echo URL; ?>public/js/jquery.min.js"></script>
    <script src="<?php echo URL; ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo URL; ?>public/js/jquery.validate.min.js"></script>
    <script src="<?php echo URL; ?>public/js/additional-methods.min.js"></script>
</head>
<body>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1><span class="glyphicon glyphicon-briefcase"></span>&nbsp; System <small>/ Rejestracja</small></h1><hr />
        <div class="error">

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-md-offset-2">
        <h2>Dane osobowe</h2>
        <form role="form" id="register" method="post" action="register/create">
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" name="name" pattern="^[A-Za-ząćęłńóśźżĄĘŁŃÓŚŹŻ]{3,20}$" autofocus="autofocus" placeholder="Imię" required/>
                    <span class="help-block">3-20 znaków</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" name="surname" pattern="^[A-Za-ząćęłńóśźżĄĘŁŃÓŚŹŻ]{3,20}$" placeholder="Nazwisko" required/>
                    <span class="help-block">3-20 znaków</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required/>
                    <span class="help-block">przykład@init.pl</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="tel" class="form-control" name="tel" pattern="^[0-9_]{9,12}$" placeholder="Telefon" required/>
                    <span class="help-block">9-12 cyfr, format: 000000000</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="password" class="form-control" name="pass1" pattern="(?=^.{8,14}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" placeholder="Hasło" required/>
                    <span class="help-block">8-14 znaków, jednak duża litera, jedna mała litera, znak specjalny lub liczba</span>
                </div>
<!--                </div> Password (UpperCase, LowerCase, Number/SpecialChar and min 8 Chars)-->
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="password" class="form-control" name="pass2" pattern="(?=^.{8,14}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" placeholder="Powtórz hasło" required/>
                    <span class="help-block">8-14 znaków, jednak duża litera, jedna mała litera, znak specjalny lub liczba</span>
                </div>
            </div>
    </div>
    <div class="col-md-4">
        <h2>Dane adresowe</h2>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" name="street" pattern="^[A-Za-ząćęłńóśźżĄĘŁŃÓŚŹŻ]{3,20}$" placeholder="Nazwa ulicy" required/>
                    <span class="help-block">3-20 liter</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" name="home_number" pattern="^[0-9_]{1,6}$" placeholder="Numer domu" required/>
                    <span class="help-block">1-6 cyfr</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" name="code_number" pattern="^[0-9]{2}-[0-9]{3}$" placeholder="Kod pocztowy" required/>
                    <span class="help-block">przykład: 22-200</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" name="city" pattern="^[A-Za-ząćęłńóśźżĄĘŁŃÓŚŹŻ]{3,20}$" placeholder="Miejscowość" required/>
                    <span class="help-block">3-20 liter</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group col-md-12">
                    <?php
                        echo("<select class=\"form-control\" name=\"country\">");
                        foreach ($country as $row) {
                            echo("<option value=\"$row->idCountry\">" . $row->countryName . "</option>");
                        }
                        echo("</select>");
                    ?>
                </div>
            </div>

        <input type="submit" class="btn btn-primary" value="Zarejestruj"/>
        <a href="login">Wróć do logowania</a>
        <input type="hidden" id="sol" value="<?php echo($this->sol); ?>"/>
        </form>
    </div>
</div>
<script src="public/js/sha256.js"></script>
<script>
    jQuery.validator.setDefaults({
        debug: true
    });
        var correct = null;

        $("#email").blur(function () {
            var getLogin = {
                'mail' : $('input[name=email]').val()
            };

            $('.error').empty();

            $.ajax({
                url: "register/check",
                type: "POST",
                data: getLogin,
                dataType: 'json',
                encode: true
            }).done(function(info) {
                $('.error').append('<div style=\'color: red\'>' + info +' </div>');
            });
        });

        $("#pass2").blur(function () {
            if($(this).val() == $("#pass1").val()) {
                console.log("Poprawne hasla");
                correct = true;
            }
        });


        $('#register').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 20
                },
                surname: {
                    required: true,
                    minlength: 3,
                    maxlength: 20
                },
                email: {
                    required: true,
                    email: true
                },
                tel: {
                    required: true,
                    minlength: 9,
                    maxlength: 12
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 14
                },
                street: {
                    required: true,
                    minlength: 3,
                    maxlength: 30
                },
                home_number: {
                    required: true,
                    minlength: 1,
                    maxlength: 10
                },
                code_number: {
                    required: true,
                    minlength: 2,
                    maxlength:10
                },
                city: {
                    required: true,
                    minlength: 3,
                    maxlength: 30
                },
                country: {
                    required: true,
                    minlength: 3,
                    maxlength: 30
                }
            },
            submitHandler: function(form) {
                var formData = $(form).serializeArray();
                // Find and replace `content` if there
//                    console.log(formData);
//                    console.log($('input[name=pass1]').val());
//                    console.log($('input[id=sol]').val());
//                    console.log(Sha256.hash($('input[name=pass1]').val() + $('input[id=sol]').val()));

                for (index = 0; index < formData.length; ++index) {
                        if ((formData[index].name == "pass1")) {
                            formData[index].value = Sha256.hash($('input[name=pass1]').val() + $('input[id=sol]').val());
                            break;
                        }
                }
                for (index = 0; index < formData.length; ++index) {
                    if ((formData[index].name == "pass2")) {
                        formData[index].value = Sha256.hash($('input[name=pass2]').val() + $('input[id=sol]').val());
                        break;
                    }
                }
                //console.log(formData);

                $.ajax({
                    url: "register/create",
                    type: "POST",
                    data: formData,
                    dataType: 'json',
                    encode: true
                    }).always(function(info) {
                        $('.error').empty();
                        if(info) {
                            $('.error').append('<div style=\'color: green\'>Poprawnie zarejestrowano.</div>');
                        } else {
                            $('.error').append('<div style=\'color: red\'>Błąd rejestracji!</div>');
                        }
                    });
            }
        });
</script>
</body>
</html>