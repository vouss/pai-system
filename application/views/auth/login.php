<!DOCTYPE html>
<html>
<head lang="pl">
    <meta charset="UTF-8">
    <title>SystemINiT</title>
    <link href="<?php echo URL; ?>public/css/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo URL; ?>public/js/jquery.min.js"></script>
    <script src="<?php echo URL; ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo URL; ?>public/js/jquery.validate.min.js"></script>
    <script src="<?php echo URL; ?>public/js/additional-methods.min.js"></script>
</head>
<body>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>Witaj w InITbiz Ltd.</h1><hr />
            <p class="lead">Logowanie</p>
            <div class="error">

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form method="POST" role="form" id="login">
                <div class="form-group">
                    <input type="email" class="form-control" autofocus="autofocus" id="email" placeholder="E-mail" required/>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="password" placeholder="Hasło" required/>
                </div>
                <input type="hidden" id="sol" value="<?php echo($this->sol);?>"/>
                <input type="hidden"  id="los" value="<?php echo($this->los);?>"/>
                <input type="submit" class="btn btn-primary" value="Zaloguj"/>
                <a href="register">Nie masz konta? Zarejestruj się!</a>
            </form>
        </div>
    </div>
    <script src="public/js/sha256.js"></script>
    <script>
        $(document).ready(function($) {

            var losowa = los(-10000,10000);
            //console.log(losowa);
            function los(min, max) {
                var argc = arguments.length;
                if (argc === 0) {
                    min = 0;
                    max = 2147483647;
                } else if (argc === 1) {
                    throw new Error('Warning: mt_rand() expects exactly 2 parameters, 1 given');
                } else {
                    min = parseInt(min, 10);
                    max = parseInt(max, 10);
                }
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }


            $('#login').submit(function () {

                var dataForm = {
                    'login': $('input[id=email]').val(),
                    'losowa': losowa,
                    'password':Sha256.hash(losowa + Sha256.hash($('input[id=password]').val() + $('input[id=sol]').val()))
                };


                $.ajax({
                    url: "login/log",
                    type: "POST",
                    data: dataForm,
                    dataType: 'json',
                    encode: true
                }).always(function (status) {
                    if (!status) {
                        $('.error').append('<div class="alert alert-dismissable alert-danger">' +
                                           '<button type="button" class="close" data-dismiss="alert">×</button>' +
                                           '<strong>Błąd!</strong> Wprowadź poprawne dane! ' +
                                           '</div>');
                    } else {
                        console.log(status);
                        if(status) {
                            document.location.href = "main";
                        } else {
                            document.location.href = "system-zgloszen/login";
                        }
                    }
                });
            return false;
            });
        });
    </script>
</body>
</html>