<div class="col-md-12">
    <div class="row">
        <div class="col-md-4 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Dane osobowe</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="error"></div>
                        <form role="form" id="edituser">
                            <?php

                            foreach ($this->data[0] as $row) {
                                echo("<label class=\"control-label\" for=\"name\">Imię</label>"
                                    ."<input class=\"form-control input-sm\" id=\"name\" name=\"name\" type=\"text\" value=\"$row->imie\">");
                                echo("<label class=\"control-label\" for=\"surname\">Nazwisko</label>"
                                    ."<input class=\"form-control input-sm\" id=\"surname\" name=\"surname\" type=\"text\" value=\"$row->nazwisko\">");
//                                echo("<label class=\"control-label\" for=\"email\">E-mail</label>"
//                                    ."<input class=\"form-control input-sm\" id=\"email\" type=\"text\" value=\"$row->email\" disabled>");
                                echo("<label class=\"control-label\" for=\"tel\">Telefon</label>"
                                    ."<input class=\"form-control input-sm\" id=\"tel\" name=\"tel\" type=\"text\" value=\"$row->telefon\">");
//                                echo("<label class=\"control-label\" for=\"new_pass1\">Nowe hasło</label>"
//                                    ."<input class=\"form-control input-sm\" id=\"new_pass1\" type=\"text\">");
//                                echo("<label class=\"control-label\" for=\"new_pass2\">Powtórz nowe hasło</label>"
//                                    ."<input class=\"form-control input-sm\" id=\"new_pass2\" type=\"text\">");
                            }
                            ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Dane kontaktowe</h3>
                </div>
                <div class="panel-body">
                    <?php
                    $nm = null;
                    foreach ($this->data[0] as $row) {
                        echo("<label class=\"control-label\" for=\"street\">Ulica</label>"
                            ."<input class=\"form-control input-sm\" id=\"street\" name=\"street\" type=\"text\" value=\"$row->ulica\">");
                        echo("<label class=\"control-label\" for=\"numb\">Numer domu</label>"
                            ."<input class=\"form-control input-sm\" id=\"numb\" name=\"numb\" type=\"text\" value=\"$row->numer_domu\">");
                        echo("<label class=\"control-label\" for=\"code\">Kod pocztowy</label>"
                            ."<input class=\"form-control input-sm\" id=\"code\" name=\"code\" type=\"text\" value=\"$row->kod_pocztowy\">");
                        echo("<label class=\"control-label\" for=\"city\">Miejscowość</label>"
                            ."<input class=\"form-control input-sm\" id=\"city\" name=\"city\" type=\"text\" value=\"$row->miejscowosc\">");
                        echo("<label class=\"control-label\" for=\"country\">Państwo</label>");
                        echo("<select class=\"form-control\" name=\"country\" id=\"mycountry\">");
                            foreach ($this->data[1] as $row2) {
                                echo("<option value=\"$row2->idCountry\">" . strtoupper($row2->countryName) . "</option>");
                            }
                         echo("</select>");
                        global $nm;
                        $nm = $row->idCountry;
                    }

                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-2">
            <input type="submit" class="btn btn-primary btn-lg" value="Zapisz zmiany"/>
            </form>
        </div>
    </div>
</div>
<?php
    echo("<script> $(\"#mycountry\").val('". $nm . "');</script>");
?>
<script src="<?php echo URL; ?>public/js/sha256.js"></script>
<script>


    $('#edituser').validate({
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 20
            },
            surname: {
                required: true,
                minlength: 3,
                maxlength: 20
            },
            tel: {
                required: true,
                minlength: 9,
                maxlength: 12
            },
            street: {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            numb: {
                required: true,
                minlength: 1,
                maxlength: 10
            },
            code: {
                required: true,
                minlength: 2,
                maxlength:10
            },
            city: {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            country: {
                required: true
            }
        },
        submitHandler: function(form) {
            var data = {
                'formData': {
                    'name': $('input[id=name]').val(),
                    'surname': $('input[id=surname]').val(),
                    'email': $('input[id=email]').val(),
                    'tel': $('input[id=tel]').val(),
                    'street': $('input[id=street]').val(),
                    'home_number': $('input[id=numb]').val(),
                    'code_number': $('input[id=code]').val(),
                    'city': $('input[id=city]').val(),
                    'country': $('select[id=mycountry]').val()
                }
            };

            $.ajax({
                url: "../main/profile",
                type: "POST",
                data: data,
                dataType: 'json',
                encode: true
            }).always(function(info) {
                if(info) {
                    $('.error').empty().append('<div class="alert alert-dismissable alert-success">'
                    + '<button type="button" class="close" data-dismiss="alert">×</button>'
                    + '<strong>Wspaniale!</strong>&nbsp; Twoje dane zostały zmienione.</a>'
                    + '</div>');
                } else {
                    $('.error').empty().append('<div class="alert alert-dismissable alert-danger">'
                    + '<button type="button" class="close" data-dismiss="alert">×</button>'
                    + '<strong>Błąd!</strong>&nbsp; Błąd!.</a>'
                    + '</div>');
                }
                return false;
            });
            return false;

        }
    });
</script>

