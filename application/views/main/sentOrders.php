<div class="col-md-10 col-md-offset-1" style="margin-bottom: 2px">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Twoje zamówienia</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <table class="table table-striped table-hover ">
                    <thead style="border-bottom: 3px solid #BF5A16">
                        <tr>
                            <th>Id zgloszenia</th>
                            <th>Data utworzenia</th>
                            <th>Tytuł</th>
                            <th>Opis</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($this->data as $row) {
                        echo("<tr>");
                        echo("<td>$row->id_zgloszenia</td>");
                        echo("<td>$row->data_utworzenia</td>");
                        echo("<td>$row->tytul</td>");
                        echo("<td>$row->opis</td>");
                        echo("<td>$row->tresc_statusu</td>");
                        echo("</tr>");
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>