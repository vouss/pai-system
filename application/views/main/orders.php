<div class="col-md-12" id="cont">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Zgłoszenia</h3>
            <div class="error"></div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <table class="table table-striped table-hover" style="color: white;">
                    <thead style="border-bottom: 3px solid #BF5A16">
                    <tr>
                        <th width="5%">ID</th>
                        <th width="45%">Tytuł usługi</th>
                        <th width="10%">Nazwisko</th>
                        <th width="10%">Imię</th>
                        <th width="10%">Data wysłania</th>
                        <th width="10%">Status</th>
                        <th width="10%">Akcja</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($this->data as $row) {
                                echo("<tr class='order'>
                                        <td>$row->id_zgloszenia</td>
                                        <td>$row->tytul</td>
                                        <td>$row->nazwisko</td>
                                        <td>$row->imie</td>
                                        <td>$row->data_utworzenia</td>
                                        <td>$row->tresc_statusu</td>
                                        <td>
                                            <span class='glyphicon glyphicon-ok-circle' onclick='confOrder($row->id_zgloszenia)' style='cursor: pointer'></span>
                                            <span class='glyphicon glyphicon-remove-sign' onclick='showModalRemove($row->id_zgloszenia)'  style='cursor: pointer'></span>
                                        </td>
                                    </tr>");
                                echo("<tr id=\"$row->id_zgloszenia\" class='more' style='background-color: #485563'>
                                        <td></td>
                                        <td><b>OPIS: </b>$row->opis</td>
                                        <td><b>EMAIL: </b>$row->email</td>
                                        <td><b>TELEFON: </b>$row->telefon</td>
                                        <td></td>
                                    </tr>");
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="modalPlace"></div>
</div>

<script>
    $('.more').hide();
    $('.order').click(function () {
        id = $(this).next().prop('id');
        $("#"+id).toggle("fast");

    });
    function confOrder(id) {
        dat = {
            'id' : id,
            'action' : 'zatwierdz'
        };

        $.ajax({
            url: "../main/orders",
            type: "POST",
            dataType: 'json',
            data: dat,
            encode: true
        }).always(function(info) {
           console.log(info);
            location.reload();
        });
    }

    function showModalRemove(id) {

        $("#modalPlace").empty();

        $('#modalPlace').append('<div class="modal" id="removeModal">'
        +'<div class="modal-dialog">'
        +'<div class="modal-content">'
        +'<div class="modal-header" style="background-color: #BF5A16">'
        +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
        +'<h4 class="modal-title">Odrzucenie zgłoszenia</h4>'
        +'</div>'
        +'<div class="modal-body">'
        +'<p>Jesteś pewien, że chcesz odrzucić wybrane <b>zgłoszenie</b> ?</p>'
        +'</div>'
        +'<div class="modal-footer">'
        +'<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>'
        +'<button type="button" class="btn btn-primary" onclick="removeOrder(' + id + ')">Tak</button>'
        +'</div>'
        +'</div>'
        +'</div>'
        +'</div>');

        $('#removeModal').modal('show');
    }

    function removeOrder(id) {
        dat = {
            'id' : id,
            'action' : 'usun'
        };

        $.ajax({
            url: "../main/orders",
            type: "POST",
            dataType: 'json',
            data: dat,
            encode: true
        }).always(function(info) {
            if(!info) {
                $('.error').empty().append('<div class="alert alert-dismissable alert-danger">'
                + '<button type="button" class="close" data-dismiss="alert">×</button>'
                + '<strong>Błąd!</strong>&nbsp; Problem z usunięciem zgłoszenia!</a>'
                + '</div>');
            }
            $('#removeModal').modal('hide');
            location.reload();
        });
    }
</script>
