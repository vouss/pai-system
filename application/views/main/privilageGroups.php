<div class="col-md-12" style="margin-bottom: 2px">
    <div class="col-md-3 col-md-offset-1">
    </div>
</div>
<div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Tabela uprawnień</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="badge"><b>Wartość</b></span>
                        <b>Uprawnienia</b>
                    </li>
                    <li class="list-group-item operator">
                        <span class="badge">512</span>
                        Otrzymane zgłoszenia od klientów
                    </li>
                    <li class="list-group-item operator">
                        <span class="badge">256</span>
                        Wszystkie projekty w systemie
                    </li>
                    <li class="list-group-item operator">
                        <span class="badge">128</span>
                        Usługi świadczone przez firmę
                    </li>
                    <li class="list-group-item operator">
                        <span class="badge">64</span>
                        Użytkownicy
                    </li>
                    <li class="list-group-item operator">
                        <span class="badge">32</span>
                        Grupy projektowe
                    </li>
                    <li class="list-group-item operator">
                        <span class="badge">16</span>
                        Kategorie użytkowników
                    </li>
                    <li class="list-group-item pracownik">
                        <span class="badge">8</span>
                        Grupy konkretnego pracownika
                    </li>
                    <li class="list-group-item pracownik">
                        <span class="badge">4</span>
                        Projekty konkretnego pracownika
                    </li>
                    <li class="list-group-item klient">
                        <span class="badge">2</span>
                        Wysyłanie zamówień
                    </li>
                    <li class="list-group-item klient">
                        <span class="badge">1</span>
                        Wyświetlanie zamówień złożonych przez konkretnego klienta
                    </li>
                    <li class="list-group-item text-right">
                        <span class="badge"><b>1023</b></span>
                        <i>SUMA</i>
                    </li>
                </ul>
                <div class="col-md-1 operator">&nbsp;</div> <div class="col-md-3">Operator</div>
                <div class="col-md-1 pracownik">&nbsp;</div> <div class="col-md-3">Pracownik</div>
                <div class="col-md-1 klient">&nbsp;</div> <div class="col-md-3">Klient</div>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Grupy</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <table class="table table-striped">
                    <thead style="border-bottom: 3px solid #BF5A16">
                    <tr>
                        <th>ID grupy</th>
                        <th>Nazwa grupy</th>
                        <th>Uprawnienia</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($this->data as $row) {
                        echo("<tr>");
                        echo("<td>$row->id_grupy</td>");
                        echo("<td>$row->nazwa_grupy</td>");
                        echo("<td>$row->uprawnienia</td>");
                        echo("</tr>");
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>