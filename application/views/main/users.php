<div class="col-md-12" style="margin-bottom: 2px">
<!--    <div class="col-md-1" style="padding-left: 0">-->
<!--        <a class="btn btn-primary" id="showModalAdd" onclick="generateModal('dodaj',null)"><span class="glyphicon glyphicon-plus"></span> &nbsp;Dodaj użytkownika</a>-->
<!--    </div>-->
    <div class="col-md-3 col-md-offset-1">
        <div class="error"></div>
    </div>
</div>
<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Użytkownicy</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <table class="table table-striped table-hover ">
                    <thead style="border-bottom: 3px solid #BF5A16">
                        <tr>
                            <th>ID</th>
                            <th>Imię</th>
                            <th>Nazwisko</th>
                            <th>Nazwa grupy</th>
                            <th>E-mail</th>
                            <th>Telefon</th>
                            <th>Ulica</th>
                            <th>Numer domu</th>
                            <th>Kod pocztowy</th>
                            <th>Miejscowość</th>
                            <th>Państwo</th>
                            <th>Akcja</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        //TODO dsadsadasdasdsa
                    foreach ($this->data[0] as $row) {
                        echo("<tr>");
                        echo("<td>$row->id_uzytkownika</td>");
                        echo("<td>$row->imie</td>");
                        echo("<td>$row->nazwisko</td>");
                        echo("<td>$row->nazwa_grupy</td>");
                        echo("<td>$row->email</td>");
                        echo("<td>$row->telefon</td>");
                        echo("<td>$row->ulica</td>");
                        echo("<td>$row->numer_domu</td>");
                        echo("<td>$row->kod_pocztowy</td>");
                        echo("<td>$row->miejscowosc</td>");
                        echo("<td>$row->countryName</td>");
                        echo("<td><span class='glyphicon glyphicon-remove-sign' onclick='showModalRemove($row->id_uzytkownika)' style='cursor: pointer;'></span>" .
                            "&nbsp<span class='glyphicon glyphicon-edit' onclick=\"generateModal('edytuj'," . $row->id_uzytkownika. ")\" style='cursor: pointer;'></span></td>");
                        echo("</tr>");
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="modalPlace"></div>
<script>
    var data = {};
    var content = {};

    function generateModal(type, id=null) {

         if (type == 'edytuj') {
            data = {
                'id': id,
                'action': 'pobierz'
            };

            $.ajax({
                url: "../main/users",
                type: "POST",
                data: data,
                dataType: 'json',
                encode: true
            }).always(function (data) {
                printModal(data);
            });
        }
    }

    function printModal(content) {

        $("#modalPlace").empty();

        $('#modalPlace').append('<div class="modal" id="userModal">'
        +'<div class="modal-dialog" style="width: 90%">'
            +'<div class="modal-content">'
                +'<div class="modal-header" style="background-color: #BF5A16">'
                    +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
                    +'<h6 class="modal-title">' + '</h6>'
                +'</div><form>'
                +'<div class="modal-body">'
                    +'<div class="row">'
                        +'<div class="col-md-4">'
                            +'<h2>Dane osobowe</h2>'
                            +'<form role="form" id="register" method="post" action="register/create">'
                            +'<div class="form-group">'
                                    +'<input type="text" class="form-control" id="name" pattern="^[A-Za-ząćęłńóśźżĄĘŁŃÓŚŹŻ]{3,20}$" autofocus="autofocus" placeholder="Imię" value=\'' + content[0].imie + '\' required/>'
                            +'</div>'
        + '<div class="form-group">'
        + '<input type="text" class="form-control" id="surname" pattern="^[A-Za-ząćęłńóśźżĄĘŁŃÓŚŹŻ]{3,20}$" placeholder="Nazwisko" value=\'' + content[0].nazwisko + '\' required/>'
        + '</div>'
        + '<div class="form-group">'
        + '<input type="email" class="form-control" id="email" placeholder="E-mail" value=\'' + content[0].email + '\' required/>'
        + '</div>'
        + '<div class="form-group">'
        + '<input type="tel" class="form-control" id="tel" pattern="^[0-9_]{9,12}$" placeholder="Telefon" value=\'' + content[0].telefon + '\' required/>'
        + '</div>'
        + '</div>'
        + '<div class="col-md-4">'
            + '<h2>Dane adresowe</h2>'
            + '<div class="form-group">'
                    + '<input type="text" class="form-control" id="street" pattern="^[A-Za-ząćęłńóśźżĄĘŁŃÓŚŹŻ]{3,20}$" placeholder="Nazwa ulicy" value=\'' + content[0].ulica + '\' required/>'
            + '</div>'
            + '<div class="form-group">'
                    + '<input type="text" class="form-control" id="home_number" pattern="^[0-9_]{1,6}$" placeholder="Numer domu" value=\'' + content[0].numer_domu + '\' required/>'
            + '</div>'
            + '<div class="form-group">'
                    + '<input type="text" class="form-control" id="code_number" placeholder="Kod pocztowy" value=\'' + content[0].kod_pocztowy + '\' required/>'
            + '</div>'
            + '<div class="form-group">'
                    + '<input type="text" class="form-control" id="city" pattern="^[A-Za-ząćęłńóśźżĄĘŁŃÓŚŹŻ]{3,20}$" placeholder="Miejscowość" value=\'' + content[0].miejscowosc + '\' required/>'
            + '</div>'
            + '<div class="form-group">'
                    + '<div class="myCountry"></div> '
            + '</div>'
        + '<input type="button" class="btn btn-primary" value="Zapisz" onclick="force(\'edytuj\',' + content[0].id_uzytkownika +')"/>'
        + ' </form>'
        + '</div>'
        + '<div class="col-md-4">'
        +'<form role="form">'
        + '<h2>Grupa</h2>'
        + '<div class="mySel"></div><div class="form-group"></div>'
        + '<input type="button" class="btn btn-primary" value="Zmień grupe" onclick="changeGroup(' + content[0].id_uzytkownika  + ')" />'
        + '</form>'
//        + '<form role="form" method="post">'
//        + '<h2>Hasło</h2>'
//        + '<div class="form-group">'
//        + '<input type="password" class="form-control" name="pass1" placeholder="Nowe hasło"/><br />'
//        + '<input type="password" class="form-control" name="pass2" placeholder="Powtórz nowe hasło"/><div class="form-group"></div>'
//        + '<input id="changePassword" type="button" class="btn btn-primary" value="Zmień hasło"/>' //TODO dodanie obslugi zmiany hasla dla uzytkownikow przez administratora
//        + '</div></div></form>'
        + '</div></div></div></div></div></div>');
        $('#userModal').modal('show');
        showSelect();
        showSelect2();
        $("#mojePanstwo").val(content[0].idCountry);
    }

    function showSelect() {
        $('.mySel').append('<?php
                                echo("<select class=\"form-control\" name=\"grupa\">");
                                foreach ($this->data2 as $row) {
                                    echo("<option value=\"$row->nazwa_grupy\">" . $row->nazwa_grupy . "</option>");
                                }
                                 echo("</select>");
                            ?>');

    }

    function showSelect2() {
        $('.myCountry').append('<?php
                                    echo("<select class=\"form-control\" id=\"mojePanstwo\">");
                                    foreach ($this->data[1] as $row) {
                                        echo("<option value=\"$row->idCountry\">" . strtoupper($row->countryName) . "</option>");
                                    }
                                     echo("</select>");
                                ?>');

    }

    function changeGroup(id) {

        data = {
            'id' : id,
            'action': 'zmienGrupe',
            'group' : $('select[name=grupa]').val()
        };
        //console.log(data);
        $.ajax({
            url: "../main/users",
            type: "POST",
            data: data,
            dataType: 'json',
            encode: true
        }).always(function (data) {
            console.log(data);
            location.reload();
        });
    }

    $('#changePassword').click(function () {
//        data = {
//            'id' : id,
//            'action': 'zmienGrupe',
//            'group' : $('select[name=grupa]').val()
//        };
//        //console.log(data);
//        $.ajax({
//            url: "../main/users",
//            type: "POST",
//            data: data,
//            dataType: 'json',
//            encode: true
//        }).always(function (data) {
//            console.log(data);
//            location.reload();
//        });
    console.log('chng pass');
    });



    function force(action,id = null) {
        data = {};

        if (action == 'edytuj') {
            data = {
                'id': id,
                'action': 'edytuj',
                'formData': {
                    'name': $('input[id=name]').val(),
                    'surname': $('input[id=surname]').val(),
                    'email': $('input[id=email]').val(),
                    'tel': $('input[id=tel]').val(),
                    'street': $('input[id=street]').val(),
                    'home_number': $('input[id=home_number]').val(),
                    'code_number': $('input[id=code_number]').val(),
                    'city': $('input[id=city]').val(),
                    'country': $('select[id=mojePanstwo]').val()
                }
            }
        }

        $.ajax({
            url: "../main/users",
            type: "POST",
            data: data,
            dataType: 'json',
            encode: true
        }).always(function (data) {

            if(data) {
                $('#removeModal').modal('hide');

                location.reload();
            }

            $('.error').empty().append('<div class="alert alert-dismissable alert-danger">'
            + '<button type="button" class="close" data-dismiss="alert">×</button>'
            + '<strong>Błąd!</strong>&nbsp; Problem z edycją użytkownika!</a>'
            + '</div>');

        });
    }


    function showModalRemove(id) {

        $("#modalPlace").empty();

        $('#modalPlace').append('<div class="modal" id="removeModal">'
        +'<div class="modal-dialog">'
        +'<div class="modal-content">'
        +'<div class="modal-header" style="background-color: #BF5A16">'
        +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
        +'<h4 class="modal-title">Usunięcie użytkownika</h4>'
        +'</div>'
        +'<div class="modal-body">'
        +'<p>Jesteś pewien, że chcesz usunąć wybranego <b>użytkownika</b> ?</p>'
        +'</div>'
        +'<div class="modal-footer">'
        +'<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>'
        +'<button type="button" class="btn btn-primary" onclick="removeUser(' + id + ')">Usuń</button>'
        +'</div>'
        +'</div>'
        +'</div>'
        +'</div>');

        $('#removeModal').modal('show');
    }

    function removeUser(id) {

        data = {
            'id' : id,
            'action' : 'usun'
        };
        console.log(data);
        $.ajax({
            url: "../main/users",
            type: "POST",
            dataType: 'json',
            data: data,
            encode: true
        }).always(function(info) {
            if(!info) {
                $('.error').empty().append('<div class="alert alert-dismissable alert-danger">'
                + '<button type="button" class="close" data-dismiss="alert">×</button>'
                + '<strong>Błąd!</strong>&nbsp; Problem z usunięciem użytkownika!</a>'
                + '</div>');
            }
            $('#removeModal').modal('hide');
            location.reload();
        });
    }

</script>