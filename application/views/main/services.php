<div class="col-md-12" style="margin-bottom: 2px">
    <div class="col-md-1" style="padding-left: 0">
        <a class="btn btn-primary" id="showModalAdd" onclick="generateModal('dodaj',null)"><span class="glyphicon glyphicon-plus"></span> &nbsp; Dodaj usługę</a>
    </div>
    <div class="col-md-3 col-md-offset-1">
        <div class="error"></div>
    </div>
</div>
<div class="col-md-12" id="cont">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Usługi</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <table class="table table-striped table-hover">
                        <thead style="border-bottom: 3px solid #BF5A16">
                            <tr>
                                <th width="5%">ID</th>
                                <th width="35%">Tytuł usługi</th>
                                <th width="55%">Opis usługi</th>
                                <th width="5%">Akcja</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($this->data as $row) {
                                echo("<tr>
                                        <td>$row->id_uslugi</td>
                                        <td>$row->tytul_uslugi</td>
                                        <td>$row->opis_uslugi</td>
                                      <td><span class='glyphicon glyphicon-remove-sign' onclick=\"showModalRemove($row->id_uslugi)\" style='cursor: pointer;'></span>
                                         &nbsp<span class='glyphicon glyphicon-edit'  onclick=\"generateModal('edytuj',$row->id_uslugi)\" style='cursor: pointer;'></span></td>
                                      </tr>");
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
<div id="modalPlace"></div>
<script>
    var data = {};
    var content = {};

    function generateModal(type, id=null) {
        var inputval = {
            'val1': '',
            'val2': ''
        };

        if (type == 'dodaj') {
            content = {
                'title': 'Dodanie usługi',
                'input1': {
                    'title': 'Tytuł usługi',
                    'id': 'title'
                },
                'input2': {
                    'title': 'Opis usługi',
                    'id': 'description'
                },
                'button': {
                    'title': 'Dodaj',
                    'id': 'dodaj'
                }
            };
            printModal(content, inputval);

        } else if (type == 'edytuj') {
            data = {
                'id': id,
                'action': 'pobierz'
            };

            content = {
                'title': 'Edytowanie usługi',
                'input1': {
                    'title': 'Tytuł usługi',
                    'id': 'title'
                },
                'input2': {
                    'title': 'Opis usługi',
                    'id': 'description'
                },
                'button': {
                    'title': 'Edytuj',
                    'id': 'edytuj'
                },
                'idElement': id
            };

            $.ajax({
                url: "../main/services",
                type: "POST",
                data: data,
                dataType: 'json',
                encode: true
            }).always(function (data) {
                for(i in data) {
                    inputval.val1 = data[i].tytul_uslugi;
                    inputval.val2 = data[i].opis_uslugi ;
                }
                printModal(content, inputval);
            });
        }


    }

    function printModal(content,inputval) {
        $("#modalPlace").empty();

        $('#modalPlace').append('<div class="modal" id="serviceModal">'
        +'<div class="modal-dialog">'
        +'<div class="modal-content">'
        +'<div class="modal-header" style="background-color: #BF5A16">'
        +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
        +'<h4 class="modal-title">' + content.title +'</h4>'
        +'</div><form>'
        +'<div class="modal-body">'
        +'<div class="form-group">'
        +'<label class="control-label" for="'+ content.input1.id+ '">' + content.input1.title + '</label>'
        +'<input class="form-control input-sm" id="'+ content.input1.id+ '" type="text"  value="' + inputval.val1 + '" required>'
        +'</div>'
        +'<div class="form-group">'
        +'<label class="control-label\" for="'+ content.input2.id+ '">'+ content.input2.title + '</label>'
        +'<input class="form-control input-sm" id="'+ content.input2.id+ '" type="text" value="' + inputval.val2 + '" required>'
        +'</div>'
        +'</div>'
        +'<div class="modal-footer">'
        +'<input type="button" class="btn btn-danger" data-dismiss="modal" value="Zamknij"/>'
        +'<input type="button" class="btn btn-primary" value="' + content.button.title + '" onclick="force(\'' + content.button.id + '\', \'' + content.idElement + ' \')"/>'
        +'</form>'
        +'</div>');
        $('#serviceModal').modal('show');
    }

    function force(action,id = null) {
        data = {};

        if (action == 'dodaj') {
            data = {
                'id': null,
                'action': 'dodaj',
                'formData': {
                    'title': $('input[id=title]').val(),
                    'des': $('input[id=description]').val()
                }
            }
        } else if (action == 'edytuj') {
            data = {
                'id': id,
                'action': 'edytuj',
                'formData': {
                    'title': $('input[id=title]').val(),
                    'des': $('input[id=description]').val()
                }
            };
        }

        $.ajax({
            url: "../main/services",
            type: "POST",
            data: data,
            dataType: 'json',
            encode: true
        }).done(function (info) {
            console.log('ok');
            $('#removeModal').modal('hide');

            location.reload();
        }).fail(function () {
            console.log("fail");
        });
    }


    function showModalRemove(id) {

        $("#modalPlace").empty();

        $('#modalPlace').append('<div class="modal" id="removeModal">'
            +'<div class="modal-dialog">'
            +'<div class="modal-content">'
            +'<div class="modal-header" style="background-color: #BF5A16">'
            +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
            +'<h4 class="modal-title">Usunięcie usługi</h4>'
            +'</div>'
            +'<div class="modal-body">'
            +'<p>Jesteś pewien, że chcesz usunąć wybraną <b>usługę</b> ?</p>'
            +'</div>'
            +'<div class="modal-footer">'
            +'<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>'
            +'<button type="button" class="btn btn-primary" onclick="removeService(' + id + ')">Usuń</button>'
            +'</div>'
            +'</div>'
            +'</div>'
            +'</div>');

        $('#removeModal').modal('show');
    }

    function removeService(id) {

        data = {
            'id' : id,
            'action' : 'usun'
        };
        console.log(data);
        $.ajax({
            url: "../main/services",
            type: "POST",
            dataType: 'json',
            data: data,
            encode: true
        }).always(function(info) {

            if(!info) {
                $('.error').empty().append('<div class="alert alert-dismissable alert-danger">'
                + '<button type="button" class="close" data-dismiss="alert">×</button>'
                + '<strong>Błąd!</strong>&nbsp; Błędne hasło.</a>'
                + '</div>');
            }
            $('#removeModal').modal('hide');
            location.reload();
        });
    }

</script>