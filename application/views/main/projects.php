<div class="col-md-12" id="cont">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Projekty</h3>
            <div class="error"></div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <table class="table table-striped table-hover" style="color: white;">
                    <thead style="border-bottom: 3px solid #BF5A16">
                    <tr>
                        <th width="10%">ID projektu</th>
                        <th width="10%">ID klienta</th>
                        <th width="10%">Data Utworzenia</th>
                        <th width="40%">Nazwa</th>
                        <th width="10%">ID zgloszenia</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($this->data as $row) {
                        echo("<tr class='order'>
                                        <td>$row->id_projektu</td>
                                        <td>$row->id_klienta</td>
                                        <td>$row->data_utworzenia</td>
                                        <td>$row->nazwa</td>
                                        <td>$row->id_zgloszenia</td>
                                    </tr>");
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>