<div class="col-md-6 col-md-offset-3" style="margin-bottom: 2px">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Wyślij zamówienie</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">

                <div class="col-md-12">
                    <div class="error"></div>
                    <form role="form" id="sendForm">
                        <select class="form-control" name="type" id="ordertype">
<!--                            <option value="0">Niestandardowe zgłoszenie</option>-->
                            <?php

                            foreach($this->data as $row) {
                                echo("<option value='" . $row->id_uslugi . "'>" . $row->tytul_uslugi . " - " . $row->opis_uslugi . "</option>");
                            }
                            ?>
                        </select>
                        <hr/>
                        <input class="form-control" style="width: 80%; margin-bottom: 10px;" id="title" name="title" placeholder="Tytuł usługi" disabled required/>
                        <textarea name="op" id="opis" class="form-control" style="width: 80%;" placeholder="Opis usługi" disabled required></textarea><br/>
                        <input type="submit" class="btn btn-primary" value="Wyślij" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        var arr = null;
        $('#ordertype').change(function () {
            $('select option:selected').each(function () {
                var option = $(this).text();
                arr = option.split(' - ')
                if(arr[0] == 'Niestandardowe zgłoszenie') {
                    $('#title').val("").prop('disabled', false);
                    $('#opis').text("").prop('disabled', false);
                } else {
                    $('#title').val(arr[0]).prop('disabled', true);
                    $('#opis').text(arr[1]).prop('disabled', true);
                }
            });
        });


        $('#sendForm').validate({
            rules: {
                title: {
                    required: true,
                    minlength: 5,
                    maxlength: 40
                },
                op: {
                    required: true,
                    minlength: 5,
                    maxlength: 120
                }
            },
            submitHandler: function(form) {

                var data = {
                    'action' : 'dodaj',
                    'id' : $('select[name="type"]').val(),
                    'title' : $("#title").val(),
                    'opis' : $("#opis").val()
                };

//                console.log(data);
                $.ajax({
                    url: "../main/sendOrder",
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    encode: true
                }).always(function (info) {

                    $('.error').append('<div class="alert alert-dismissable alert-success">' +
                    '<button type="button" class="close" data-dismiss="alert">×</button>' +
                    '<strong>Gratulacje!</strong> Wysłano zgłoszenie.'+
                    '</div>');
                });
                return false;
            }
        });
    });

</script>