<div class="col-md-3">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Dodaj grupe do projektu</h3>
        </div>
        <div class="panel-body">
            <div class="form-group text-right">
                <form role="form" id="addform" method="post">
                    <input type="text" class="form-control" name="groupName" placeholder="Nazwa grupy"/><br/>
                    <?php
                        echo("<select class=\"form-control\" name=\"proj\" id=\"projects\">");
                        foreach ($this->data2[1] as $row) {
                            echo("<option value=\"$row->id_projektu\">" . $row->nazwa . "</option>");
                        }
                    echo("</select><br />");
                    ?>
                    <input type="submit" class="btn btn-primary" value="Dodaj"/>
                </form>
            </div>
        </div>
    </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Przydziel pracownika</h3>
            </div>
            <div class="panel-body">
                <div class="form-group text-right">
                    <form>
                        <div id="selWorkers"></div>
                        <div id="selProjGroups">
                            <?php
                                echo("<select class=\"form-control\" id=\"groups\">");
                                foreach ($this->data as $row) {
                                    echo("<option value=\"$row->id_grupy\">" . strtoupper($row->nazwa) . "</option>");
                                }
                                echo("</select><br />");
                            ?>
                        </div>
                        <input type="button" class="btn btn-primary"  id="przydziel" value="Przydziel"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Grupy projektowe</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <table class="table table-striped">
                        <thead style="border-bottom: 3px solid #BF5A16">
                            <tr>
                                <th width="15%">ID</th>
                                <th>Nazwa</th>
                                <th>Akcja</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($this->data as $row) {
                                echo("<tr>
                                        <td>$row->id_grupy</td>
                                        <td onclick=\"getInfo(" . $row->id_grupy . ")\" style='cursor: pointer;'>$row->nazwa</td>
                                              <td><span class='glyphicon glyphicon-remove-sign' onclick=\"removeGroup(" . $row->id_grupy . ")\" style='cursor: pointer;'></span>
                                                 </td>
                                    </tr>");
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informacje</h3>
            </div>
            <div class="panel-body">
                <div class="form-group" id="inf">
                   Wybierz grupę, aby wyświetlić szczegółowe informacje.
                </div>
            </div>
        </div>
    </div>
    <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>
    jQuery.validator.setDefaults({
        debug: true
    });

    $("#przydziel").click(function () {
        //console.log($('#workers').val());
        //console.log($('#groups').val());

        var data = {
            'action' : 'przydziel',
            'person_id' : $('#workers').val(),
            'group_id' : $('#groups').val()
        };

        $.ajax({
            url: "../main/projectGroups",
            type: "POST",
            data: data,
            dataType: 'json',
            encode: true
        }).always(function (data) {
            location.reload();
        });
    });

        function showWorkersSelect() {

            $('#selWorkers').append('<?php
                                    echo("<select class=\"form-control\" id=\"workers\">");
                                    foreach ($this->data2[0] as $row) {
                                        echo("<option value=\"$row->id_uzytkownika\">" . $row->nazwisko . " " . $row->imie . "</option>");
                                    }
                                     echo("</select><br />");
                                ?>');

        }

        showWorkersSelect();

        $('#addform').validate({
            rules: {
                groupName: {
                    required: true,
                    minlength: 3,
                    maxlength: 20
                }
            },
            submitHandler: function(form) {

                var formData = $(form).serialize();
                //console.log(formData);
                $.ajax({
                    url: "../main/projectGroups",
                    type: "POST",
                    data: formData + "&action=dodaj",
                    dataType: 'json',
                    encode: true
                }).always(function (data) {
                    //console.log(data);
                    location.reload();
                });
            }
        });

        function getInfo(id) {
            dataInf = {
                'action' : 'info',
                'id' : id
            };

            $.ajax({
                url: "../main/projectGroups",
                type: "POST",
                data: dataInf,
                dataType: 'json',
                encode: true
            }).always(function (data) {
                //console.log(data);
                var head = "<table class=\"table table-striped\">";
                head += " <thead style=\"border-bottom: 3px solid #BF5A16\"> "+
                            "<tr>"+
                            "<th>Imie</th>"+
                            "<th>Nazwisko</th>"+
                            "<th>Akcja</th>"+
                            "</tr>"+
                            "</thead>"+
                            "<tbody>";
                for ( o in data ) {
                    head += "<tr><td>" + data[o].imie + "</td><td>" + data[o].nazwisko + "</td><td>" +
                    " &nbsp;<span class=\"glyphicon glyphicon-remove-sign\" onclick=\"removePersonFromGroup(" + data[o].id_uzytkownika + ","+ data[o].id_grupy +")\" style=\"cursor: pointer\"></span></td></tr>";
                }
                head += "</body></table>";
                $('#inf').html(head);
            });
        }
        function removePersonFromGroup(id_uzytkownika, id_grupy) {
            data = {
                'action' : 'usunOsobeZGrupy',
                'id_uzytkownika' : id_uzytkownika,
                'id_grupy' : id_grupy
            };

            $.ajax({
                url: "../main/projectGroups",
                type: "POST",
                data: data,
                dataType: 'json',
                encode: true
            }).always(function (data) {
                location.reload();
            });
        }

        function removeGroup(id) {
            data = {
                'action' : 'usun',
                'id' : id
            };

            $.ajax({
                url: "../main/projectGroups",
                type: "POST",
                data: data,
                dataType: 'json',
                encode: true
            }).always(function (data) {
                location.reload();
            });
        }
</script>