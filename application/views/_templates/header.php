<!DOCTYPE html>
<html>
<head lang="pl">
    <noscript>
        <meta HTTP-EQUIV="Refresh" CONTENT="0" />
    </noscript>
    <meta charset="UTF-8">
    <title>SystemINiT</title>
    <link href="<?php echo URL; ?>public/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL; ?>public/css/style.css" rel="stylesheet">
    <script src="<?php echo URL; ?>public/js/jquery.min.js"></script>
    <script src="<?php echo URL; ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo URL; ?>public/js/jquery.validate.min.js"></script>
    <script src="<?php echo URL; ?>public/js/additional-methods.min.js"></script>
</head>
<body>
