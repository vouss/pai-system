<?php

class UsersModel extends BaseModel {

    function userData($id) {

        $sql1 = "SELECT * FROM dane_uzytkownika WHERE id_uzytkownika='" . $id ."';";
        $query = $this->db->prepare($sql1);
        $query->execute();
        $rows = $query->fetchAll(PDO::FETCH_OBJ);
        return $rows;
    }
    function userCheck($mail) {

        $sql1 = "SELECT * FROM dane_uzytkownika WHERE email='" . $mail ."';";
        $query = $this->db->prepare($sql1);
        $query->execute();
        $rows = $query->fetchAll(PDO::FETCH_OBJ);
        return $rows;
    }

    function getCountry() {
        try {
            $sql1 = "SELECT * FROM panstwa;";
            $query = $this->db->prepare($sql1);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - ' . " " . $e);
        }
        return $rows;
    }

    function printAllByType($type) {

        try {
            $sql1 = "SELECT id_uzytkownika,imie,nazwisko FROM dane_uzytkownika WHERE nazwa_grupy='" . $type . "';";
            $query = $this->db->prepare($sql1);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - ' . " " . $e);
        }
        return $rows;
    }

    function printAllUsers() {

        try {
            $sql1 = "SELECT * FROM dane_uzytkownika;";
            $query = $this->db->prepare($sql1);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - ' . " " . $e);
        }
        return $rows;
    }

    public function removeUser($id) {

        try {
            $sql = "DELETE FROM uzytkownicy WHERE id_uzytkownika='" .$id ."';";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - ' . " " . $e);
        }

        return true;
    }


    function changeGroup($group_name, $user_id) { // group name unique
        var_dump($group_name);
        var_dump($user_id);
        try {
            $sql1 = "UPDATE uzytkownicy SET id_grupy = (SELECT id_grupy FROM grupy WHERE nazwa_grupy='" . $group_name . "')"
                . "WHERE id_uzytkownika ='" . $user_id . "';";
            $query = $this->db->prepare($sql1);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query -' . " " . $e);
        }
        return true;
    }

    function editUser($data) {


        try {
            $sql1 = "UPDATE uzytkownicy SET imie = '" . $data['formData']['name']. "', nazwisko = '"
                . $data['formData']['surname'] . "', telefon = '" . $data['formData']['tel'] . "'"
                . "WHERE id_uzytkownika = '" . $data['id'] . "';";
            $query = $this->db->prepare($sql1); //dodanie edycji danych KONTAKTOWYCH!!!
            $query->execute();

            $sql2 = "UPDATE adresy SET ulica = '" . $data['formData']['street']. "', numer_domu = '"
                . $data['formData']['home_number'] . "', kod_pocztowy = '" . $data['formData']['code_number']
                . "', miejscowosc = '" . $data['formData']['city']
                . "', idCountry = '" . $data['formData']['country'] . "'" //TODO edycja id_panstwa - wyswietlac select
                . "WHERE id_uzytkownika = '" .$data['id'] . "';";

            $query2 = $this->db->prepare($sql2);
            $query2->execute();
        } catch(PDOException $e) {
            exit('Problem with query -' . " " . $e);
        }
        return true;

    }

    public function addUser($data) {
        if(!$this->checkMail($data['email'])) return false;

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $imie = $this->checkInput($data['name']);
            $nazwisko = $this->checkInput($data['surname']);
            $email = $this->checkInput($data['email']);
            $telefon = $this->checkInput($data['tel']);
            $ulica = $this->checkInput($data['street']);
            $numer_domu = $this->checkInput($data['home_number']);
            $kod_pocztowy = $this->checkInput($data['code_number']);
            $miasto = $this->checkInput($data['city']);
            $panstwo = $this->checkInput($data['country']);
            $haslo = $this->checkInput($data['pass1']);

            try {
                $this->db->BeginTransaction();

//            $sql1 = "INSERT INTO uzytkownicy (imie, nazwisko, email, telefon, haslo,id_grupy)
//                VALUES('" . $data['name'] . "', '" . $data['surname'] . "', '" . $data['email']. "', '" .$data['tel'] ."', '" .$data['pass1'] . "',0);";
//            $query = $this->db->prepare($sql1);
//            $query->execute();
//            $sql2 = "INSERT INTO adresy (id_uzytkownika,ulica,numer_domu,kod_pocztowy,miejscowosc,idCountry)
//                  VALUES('". $this->db->lastInsertId() . "', '" . $data['street'] . "', '" .$data['home_number'] . "', '" . $data['code_number'] .
//                "', '" . $data['city'] . "', '" . $data['country'] ."')";
//            $query2 = $this->db->prepare($sql2);
                $sql1 = "INSERT INTO uzytkownicy (imie, nazwisko, email, telefon, haslo,id_grupy)
                VALUES(:name, :surname, :email, :tel, :pass1, 0);";
                $query = $this->db->prepare($sql1);

                $query->bindValue( "name", $imie , PDO::PARAM_STR );
                $query->bindValue( "surname", $nazwisko , PDO::PARAM_STR );
                $query->bindValue( "email", $email , PDO::PARAM_STR );
                $query->bindValue( "tel", $telefon , PDO::PARAM_INT );
                $query->bindValue( "pass1", $haslo , PDO::PARAM_STR );

                $query->execute();

                $sql2 = "INSERT INTO adresy (id_uzytkownika,ulica,numer_domu,kod_pocztowy,miejscowosc,idCountry)
                  VALUES('". $this->db->lastInsertId() . "',:street, :home_number, :code_number,:city,:country  )";
                $query2 = $this->db->prepare($sql2);
                $query2->bindValue( "street", $ulica , PDO::PARAM_STR );
                $query2->bindValue( "home_number", $numer_domu , PDO::PARAM_INT );
                $query2->bindValue( "code_number", $kod_pocztowy , PDO::PARAM_STR );
                $query2->bindValue( "city", $miasto , PDO::PARAM_STR );
                $query2->bindValue( "country", $panstwo , PDO::PARAM_INT);

                $query2->execute();
                $this->db->commit();

            } catch(PDOException $e) {
                $this->db->rollBack();
                echo $e->getMessage();
            }
        }


        return true;

    }

    private function checkInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function checkMail($email) {
        try {
            $sql = "SELECT * FROM uzytkownicy WHERE email='" . $email . "';";
            $query = $this->db->prepare($sql);
            $query->execute();
            $check = $query->fetch(PDO::FETCH_ASSOC);
        } catch(PDOException $e) {
            exit('Problem with query - ' . " " . $e);
        }

        return ($check)? false : true;
    }

    private function checkPassword($password,$id) {
        $sql1 = "SELECT haslo FROM uzytkownicy WHERE id_uzytkownika='" . $id ."';";
        $query = $this->db->prepare($sql1);
        $query->execute();
        $row = $query->fetchAll(PDO::FETCH_ASSOC);

        if($row[0]['haslo'] == $password) {
            return true;
        }
        return false;
    }
}