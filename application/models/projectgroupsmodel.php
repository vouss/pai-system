<?php

class ProjectGroupsModel extends BaseModel {

    public function printAllGroups() {

        try {
            $sql = "SELECT * FROM grupy_projektowe";
            $query = $this->db->prepare($sql);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - show ' . " " . $e);
        }

        return $rows;
    }

    public function printWorkerGroups($worker_id) {

        try {
            $sql = "SELECT * FROM system.grupy_projektowe, system.uzytkownicy_grupy_projektowe
                    WHERE grupy_projektowe.id_grupy = uzytkownicy_grupy_projektowe.id_grupy
                    AND uzytkownicy_grupy_projektowe.id_uzytkownika = " . $worker_id;
            $query = $this->db->prepare($sql);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }

        return $rows;
    }

    public function printProjectGroup($id) {

        try {
            $sql = "SELECT u.imie, u.nazwisko, u.email, u.id_uzytkownika, g.id_grupy FROM uzytkownicy u, grupy_projektowe g, uzytkownicy_grupy_projektowe ug
                      WHERE u.id_uzytkownika=ug.id_uzytkownika AND ug.id_grupy=g.id_grupy AND g.id_grupy =" . $id . ";";
            $query = $this->db->prepare($sql);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }

        return $rows;
    }

    public function addProjectGroup($name,$project_id) {

        try {
            $sql = "INSERT INTO grupy_projektowe(nazwa, id_projektu) VALUES('" . $name ."'," . $project_id .");";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }

        return true;
    }

    public function removeProjectGroup($id) {
        try {
            $sql = "DELETE FROM grupy_projektowe WHERE id_grupy='" .$id ."';";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }

        return true;
    }

    public function addPersonToProjectGroup($personID, $groupID) {

        try {
            $sql = "INSERT INTO uzytkownicy_grupy_projektowe(id_uzytkownika, id_grupy) VALUES('" . $personID ."','" . $groupID ."');";
//            $sql = "INSERT IGNORE INTO uzytkownicy_grupy_projektowe SET id_uzytkownika='" . $personID ."',id_grupy='" . $groupID ."';";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }

        return true;
    }

    public function removePersonFromProjectGroup($personID, $groupID) {
        try {
            $sql = "DELETE FROM uzytkownicy_grupy_projektowe WHERE id_uzytkownika='" . $personID ."' AND id_grupy='" . $groupID ."';";
//            $sql = "INSERT IGNORE INTO uzytkownicy_grupy_projektowe SET id_uzytkownika='" . $personID ."',id_grupy='" . $groupID ."';";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }

        return true;
    }

    public function printService($id) {

        try {
            $sql = "SELECT tytul_uslugi,opis_uslugi FROM uslugi WHERE id_uslugi='" . $id . "'";
            $query = $this->db->prepare($sql);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        } catch(PDOException $e) {
            exit('Problem with query - show services' . " " . $e);
        }

        return $rows;
    }
}