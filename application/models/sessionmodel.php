<?php
class SessionModel extends BaseModel {

    public function addSession($ciastko) {

        try {
            $sql = "INSERT INTO sesje(ciastko,do_kiedy) VALUES('" .$ciastko ."',' " . date("Y-m-d H:i:s", strtotime('+3 hours')) . "');";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - adding cookies' . " " . $e);
        }

    }

    public function checkSession($ciastko) {
        try {
            $sql = "SELECT ciastko,do_kiedy FROM sesje WHERE ciastko='" .$ciastko . "';";
            $query = $this->db->prepare($sql);
            $query->execute();
            $data = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - checking session' . " " . $e);
        }

        foreach($data as $row) {
            if($row->do_kiedy > date("Y-m-d H:i:s")) {
                return true;
            }
        }
        return false;

    }

    public function deleteSession($ciastko) {
        try {
            $sql = "DELETE FROM sesje WHERE ciastko='" .$ciastko ."';";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }
    }
}