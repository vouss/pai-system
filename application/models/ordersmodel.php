<?php

class OrdersModel extends BaseModel {

    public function printAllOrders() {

        try {
            $sql = "SELECT * FROM zamowienia_klientow";
            $query = $this->db->prepare($sql);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - show services' . " " . $e);
        }
        return $rows;
    }
    public function printOrder($id) {

        try {
            $sql = "SELECT * FROM zamowienia_klientow WHERE id_zgloszenia=" . $id;
            $query = $this->db->prepare($sql);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - show services' . " " . $e);
        }
        return $rows;
    }

    public function removeOrder($id) {
        try {
            $sql = "DELETE FROM zgloszenia WHERE id_zgloszenia='" .$id ."';";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }

        return true;
    }

    public function addOrder($data) {

        try {
            $sql = "INSERT INTO zgloszenia(id_uzytkownika,data_utworzenia,tytul,opis,id_statusu) VALUES('"
                                           . $_SESSION['id'] . "','". date("Y-m-d") . "','" .$data['title'] ."','" . $data['opis'] ."',1);";
            $query = $this->db->prepare($sql);
            $query->execute();
        } catch(PDOException $e) {
            exit('Problem with query - removing cookies' . " " . $e);
        }

        return true;

    }

    public function orderToProject($order) {

        foreach($order as $row) {

            try {
                $sql = "INSERT INTO projekty(id_klienta,data_utworzenia,nazwa,id_zgloszenia) VALUES('"
                    . $row->id_uzytkownika . "','" . date("Y-m-d") . "','" . $row->opis . " | " . $row->email . "', '" . $row->id_zgloszenia . "');";
                $query = $this->db->prepare($sql);
                $query->execute();

                $sql2 = "UPDATE zgloszenia SET id_statusu = 2 WHERE id_zgloszenia=" . $row->id_zgloszenia;
                $query = $this->db->prepare($sql2);
                $query->execute();
            } catch(PDOException $e) {
                exit('Problem with query - removing cookies' . " " . $e);
            }
        }


        return true;
    }
//    public function printOrder($id) {
//        try {
//            $sql = "SELECT tytul_uslugi,opis_uslugi FROM uslugi WHERE id_uslugi='" . $id . "'";
//            $query = $this->db->prepare($sql);
//            $query->execute();
//            $rows = $query->fetchAll(PDO::FETCH_ASSOC);
//        } catch(PDOException $e) {
//            exit('Problem with query - show services' . " " . $e);
//        }
//
//        return $rows;
//    }

    public function sentOrders($id) {
        try {
            $sql = "SELECT id_zgloszenia,id_uzytkownika, data_utworzenia, tytul, opis, tresc_statusu FROM zamowienia_klientow WHERE id_uzytkownika='" . $id . "'";
            $query = $this->db->prepare($sql);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - show services' . " " . $e);
        }

        return $rows;
    }

}