<?php

class GroupsModel extends BaseModel {

    function printAllGroups() {
        $sql1 = "SELECT * FROM grupy";
        $query = $this->db->prepare($sql1);
        $query->execute();
        $rows = $query->fetchAll(PDO::FETCH_OBJ);

        return $rows;
    }

    function printGroup($group_name) {
        $sql1 = "SELECT * FROM grupy WHERE nazwa_grupy='" . $group_name . "';";
        $query = $this->db->prepare($sql1);
        $query->execute();
        $rows = $query->fetchAll(PDO::FETCH_OBJ);

        return $rows;
    }

    function changePrivileges($group_name, $privileges) { // group name unique
        $sql1 = "UPDATE grupy SET uprawnienia  = '" . $privileges . "'WHERE nazwa_grupy ='" . $group_name . "';";
        $query = $this->db->prepare($sql1);
        $query->execute();

    }

    function changeGroupName($group_name_before, $group_name_after) { // group name unique
        $sql1 = "UPDATE grupy SET nazwa_grupy  = '" . $group_name_after . "'WHERE nazwa_grupy ='" . $group_name_before . "';";
        $query = $this->db->prepare($sql1);
        $query->execute();

    }
}
