<?php
 class BaseModel { //TODO PDO-> prevent sql injection

     function __construct($db) {

         try {
             $this->db = $db;
         } catch(PDOException $e) {
             exit('[BASEMODEL] Problem with connection to DB');
         }
     }
 }