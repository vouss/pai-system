<?php

class LoginModel extends BaseModel {

    public function login() {

        if (!empty($_POST['login']) && !empty($_POST['password'])) {

            $sql = "SELECT id_uzytkownika,imie,nazwisko, email, haslo,grupy.nazwa_grupy AS nazwa_grupy FROM uzytkownicy,grupy" .
                " WHERE grupy.id_grupy=uzytkownicy.id_grupy AND email=:login LIMIT 1;";
            $query = $this->db->prepare($sql);
            $query->bindValue( "login", $_POST['login'] , PDO::PARAM_STR );

            $query->execute();

            $row_counts = $query->rowCount();
            $row = $query->fetch(PDO::FETCH_OBJ);

            if($row_counts == 1) {

                $haslo = hash("sha256",( $_POST['losowa'] . $row->haslo));

                if($_POST['password'] == $haslo) {

                    return true;

                } else {
                    $now = date('Y-m-d H:i:s');
                    $query = $this->db->prepare("INSERT INTO proby_logowan(id_uzytkownika, data)VALUES('" .$row->id_uzytkownika .
                        "','" . $now . "')");
                    $query->execute();

                    return false;
                }
            }
        } else {
            return false;
        }
    }
}