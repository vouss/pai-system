<?php

 class ServicesModel extends BaseModel {

    public function printAllServices() {

        try {
            $sql = "SELECT id_uslugi,tytul_uslugi,opis_uslugi FROM uslugi";
            $query = $this->db->prepare($sql);
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_OBJ);
        } catch(PDOException $e) {
            exit('Problem with query - show services' . " " . $e);
        }

        return $rows;
    }

     public function removeService($id) {

         try {
             $sql = "DELETE FROM uslugi WHERE id_uslugi='" .$id ."';";
             $query = $this->db->prepare($sql);
             $query->execute();
         } catch(PDOException $e) {
             exit('Problem with query - removing cookies' . " " . $e);
         }

         return true;
     }

     public function addService($data) { //TODO zamiana na procedure w SQL...

         try {
             $sql = "INSERT INTO uslugi(tytul_uslugi, opis_uslugi) VALUES('" .$data['formData']['title'] ."','" . $data['formData']['des'] ."');";
             $query = $this->db->prepare($sql);
             $query->execute();
         } catch(PDOException $e) {
             exit('Problem with query - removing cookies' . " " . $e);
         }

         return true;
     }

     public function editService($data) {

         try {
             $sql = "UPDATE uslugi SET opis_uslugi = '" . $data['formData']['des'] . "', tytul_uslugi = '"
                 . $data['formData']['title']  . "'"
                 . "WHERE id_uslugi = '" .$data['id'] . "';";
             $query = $this->db->prepare($sql);

             $query->execute();

         } catch(PDOException $e) {
                exit('Problem with query - removing cookies' . " " . $e);
         }

         return true;
     }

     public function printService($id) {

         try {
             $sql = "SELECT tytul_uslugi,opis_uslugi FROM uslugi WHERE id_uslugi='" . $id . "'";
             $query = $this->db->prepare($sql);
             $query->execute();
             $rows = $query->fetchAll(PDO::FETCH_ASSOC);
         } catch(PDOException $e) {
             exit('Problem with query - show services' . " " . $e);
         }

         return $rows;
     }
 }