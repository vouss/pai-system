<?php
/**
 * Base Controller
 */
class Controller {
    public $db = null;
    protected $groups = null;

    public $sol = "e37f0136aa3ffaf149b351f6a4c948e9"; //sol init
    public $session = null;

    function __construct() {
        $this->openDatabaseConnection();
        $GLOBALS['sol'] = $this->sol;
    }

    public function setModelGroups() {
        $this->groups = $this->loadModel("GroupsModel");
    }

    public function __get($property) {
        if($property == "session") {
            return $this->session;
        }
    }

    protected  function isMethod($method) {
        return ($_SERVER['REQUEST_METHOD'] == $method)? true : false;
    }

    protected  function isAjax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')? true : false;
    }

    private function openDatabaseConnection() {
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS, $options);
        $this->db->query("SET NAMES 'utf8'");

    }

    public function loadModel($model_name) {
        require 'application/models/' . strtolower($model_name) . '.php';
        // return new model (and pass the database connection to the model)
        return new $model_name($this->db);
    }

}
