<?php

class Menu {

    public $header = null;
    public $priv_array = array();

    function __construct() {

        $this->priv_array = array(
            // nazwa => sciezka
            array('Wysłane zamówienia' => 'sentOrders'),
            array('Wyślij zamówienie' => 'sendOrder'),
            array('Twoje projekty' => 'workerProjects'),
            array('Twoje grupy projektowe' => 'workerProjectGroups'),
            array('Grupy uprawnieniowe' => 'privilageGroups'),
            array('Grupy projektowe' => 'projectGroups'),
            array('Użytkownicy' => 'users'),
            array('Usługi' => 'services'),
            array('Wszystkie projekty' => 'projects'),
            array('Otrzymane zgłoszenia' => 'orders'),
        );

        $this->header = '<div class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" style="border-right: 1px solid white">InIT | System</a>
                        </div>
                        <div class="navbar-collapse collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">';
    }

    public function printMenu($uprawnienia) {
        $count = 0;

        while($uprawnienia > 0) {

            //niekoniecznie > 0
            if($uprawnienia % 2 == 1) {
                //var_dump($this->priv_array[$count]);
                $this->header .="<li><a href=\"". URL . "main/".
                    array_values($this->priv_array[$count])[0] ."\">" .
                    array_keys($this->priv_array[$count])[0] . "</a></li>";
            }
            $uprawnienia = $uprawnienia >> 1;
            $count++;
        }
        $this->header .= '</ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> &nbsp;' . $_SESSION['name'] . '<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="' . URL . 'main/profile">Profil</a></li>
                                        <li class="divider"></li>
                                        <li><a href="' . URL . 'login/logout">Wyloguj</a></li>
                                    </ul>
                                </li>
                            </ul>
                            </div>
                            </div>';

        echo($this->header);
    }






}