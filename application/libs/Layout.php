<?php
 class Layout {
     public $wymagane_uprawnienia_strony = null;
     public $uprawnienia_uzytkownika = null;
     public $nazwa_widoku = null;
     public $menu = null;
     public $maskaUprawnien = null;
     public $data = null;
     public $data2 = null;

     function __construct($wymagane_uprawnienia_strony,$uprawnienia_uzytkownika, $nazwa_widoku, $data, $data2) {
         $this->wymagane_uprawnienia_strony = $wymagane_uprawnienia_strony;
         $this->uprawnienia_uzytkownika = $uprawnienia_uzytkownika;
         $this->nazwa_widoku = $nazwa_widoku;
         $this->data = $data;
         $this->data2 = $data2;
     }

     public function addMenu() {
         $this->menu = new Menu();
         $this->menu->printMenu($this->uprawnienia_uzytkownika);
     }

     public function addContent($nazwa_widoku) {

        require 'application/views/main/' . strtolower($nazwa_widoku) .'.php';
     }

     public function make() {
         require 'application/views/_templates/header.php';
         if($this->checkVisibility()) {
             $this->addMenu();
             $this->addContent($this->nazwa_widoku);
         } else {
             //widok z info o braku strony
         }

         require 'application/views/_templates/footer.php';
     }

     /*
 *
 * return true JEŻELI grupa moze obejzec strone
 */
     private function checkVisibility() {
         if($this->wymagane_uprawnienia_strony & $this->uprawnienia_uzytkownika ) {
             return true;
         } else {
             return false;
         }
     }
 }
